package com.vtaper.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.vtaper.app.model.Address;

public interface AddressRepository extends CrudRepository < Address , Long > {

}
