package com.vtaper.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.vtaper.app.model.User;

public interface UserRepository extends CrudRepository < User , Long > {

}
