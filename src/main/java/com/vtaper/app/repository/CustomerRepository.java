package com.vtaper.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.vtaper.app.model.Customer;

public interface CustomerRepository extends CrudRepository < Customer, Long >
{

}
