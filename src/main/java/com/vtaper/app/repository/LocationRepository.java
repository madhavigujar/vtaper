package com.vtaper.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.vtaper.app.model.Location;

public interface LocationRepository extends CrudRepository < Location , Long > {

}
