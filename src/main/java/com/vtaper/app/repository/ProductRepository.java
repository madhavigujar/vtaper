package com.vtaper.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.vtaper.app.model.Product;

public interface ProductRepository extends CrudRepository < Product , Long > {

}
