package com.vtaper.app.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vtaper.app.model.Customer;
import com.vtaper.app.service.CustomerService;


@Controller

public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
    @RequestMapping(value="/addcustomer",method=RequestMethod.GET)
	public ModelAndView addCustomer() {
		ModelAndView model=new ModelAndView();
		Customer customer= new Customer();
		model.addObject("customerForm",customer);
		model.setViewName("customer_form");
		
		return model;
	}

}
