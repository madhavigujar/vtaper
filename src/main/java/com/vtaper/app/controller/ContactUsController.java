package com.vtaper.app.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

    @Controller
    public class ContactUsController {
	
	
	 @Value("${contactus.message}")
	 private String message;
		    
		    
		  @GetMapping("/ContactUs")
		  public String main(Model model) {
		  model.addAttribute("message", message);
		        

		  return "contactus"; //view


}
}
