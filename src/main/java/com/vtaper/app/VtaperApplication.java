package com.vtaper.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtaperApplication {

	public static void main(String[] args) {
		SpringApplication.run(VtaperApplication.class, args);
	}

}

