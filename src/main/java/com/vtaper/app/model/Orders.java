package com.vtaper.app.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
//Indexed

public class Orders {
	
	@Id
	private long id;
	private String product_details;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProduct_details() {
		return product_details;
	}
	public void setProduct_details(String product_details) {
		this.product_details = product_details;
	}
	public Orders(long id, String product_details) {
		super();
		this.id = id;
		this.product_details = product_details;
	}

}
