package com.vtaper.app.model;

import javax.persistence.Entity;
import javax.persistence.Id;

//@Indexed
@Entity
public class Address {
	@Id
	private String id;
	private String city;
	private String pincode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Address(String id, String city, String pincode) {
		super();
		this.id = id;
		this.city = city;
		this.pincode = pincode;
	}

}
