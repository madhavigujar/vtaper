package com.vtaper.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT")
public class Product {
	@Column(name="PRODUCT_NAME")
	private String product_name;
	
	@Column(name="PRICE")
	private int price;
	
	@Id
	@Column(name="ID")
	private long id;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="PRODUCT_INFORMATION")
	private String product_information;
	
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProduct_information() {
		return product_information;
	}
	public void setProduct_information(String product_information) {
		this.product_information = product_information;
	}
	public Product(String product_name, int price, long id, String description, String product_information) {
		super();
		this.product_name = product_name;
		this.price = price;
		this.id = id;
		this.description = description;
		this.product_information = product_information;
	}
	
	
	
	

}
