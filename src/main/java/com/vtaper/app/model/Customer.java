package com.vtaper.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//Indexed
@Entity
@Table(name="CUSTOMER")
public class Customer {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
	private long id;
    
    @Column(name="FIRSTNAME")
	private String firstName;
    
    @Column(name="LASTNAME")
	private String lastName;
    
    @Column(name="EMAILID")
	private String email_id;
    
    @Column(name="CONTACTNO")
	private String contact_No;
    
    
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail_id() {
		return email_id;
	}


	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}


	public String getContact_No() {
		return contact_No;
	}


	public void setContact_No(String contact_No) {
		this.contact_No = contact_No;
	}
	
}
	