package com.vtaper.app.model;



import javax.persistence.Entity;
import javax.persistence.Id;
//@Indexed
@Entity
public class User {
	
	@Id
	private long id;
	private int contact_no;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getContact_no() {
		return contact_no;
	}
	public void setContact_no(int contact_no) {
		this.contact_no = contact_no;
	}
	public User(long id, int contact_no) {
		super();
		this.id = id;
		this.contact_no = contact_no;
	}
	
	

}
