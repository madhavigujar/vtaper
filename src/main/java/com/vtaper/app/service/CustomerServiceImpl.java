package com.vtaper.app.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vtaper.app.model.Customer;
import com.vtaper.app.repository.CustomerRepository;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerRepository customerRepository;

	@Override
	public void add(Customer customer) {
		// TODO Auto-generated method stub
		customerRepository.save(customer);
		
	}
   
   




}
