<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0.1 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=I50-8859-1">
<title>Customer Form</title>
<link href="../..Webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
<script src="../..Webjars/bootstrap/4.0.o/js/bootstrap.min.js"></script>
<script src="../..Webjars/jquuery/3.0.0/js/jquery.min.js"></script>
</head>
<body>
<div class="container">
	<spring:url value="/customer/saveCustomer" var="saveURL" />
	<h2>Customer</h2>
	
	<form:form modelAttribute="customerForm" method="post" action="${saveURL }" cssClass="form" >
	<form:hidden path="id"/>
		<div class="form-group">
		    <label>Title</label>
		    <form:input path="firstname" cssClass="form-control" id="title" />
		    
		</div>
		<div class="form-group">
		    <label>Category</label>
		    <form:input path="firstname" cssClass="form-control" id="category"/>
		 </div>
           <button type="submit" class="btn btn-primary">Save</button>
     </form:form>
</div>
</body>
</html>